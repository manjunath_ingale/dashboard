import React, { useState, useEffect } from "react";
import MaterialTable from "material-table";
import './Table.css'

const Table = props => {
  const [gridData, setGridData] = useState({
    data: props.data,
    columns: props.col,
    resolve: () => {},
    updatedAt: new Date()
  });

  useEffect(() => {
    gridData.resolve();
    console.log("RESOLVE AT:", gridData.updatedAt);
  }, [gridData]);

  const onRowAdd = newData =>
    new Promise((resolve, reject) => {
      const data = [...gridData.data];
      data.push(newData);
      const updatedAt = new Date();
      setGridData({ ...gridData, data, updatedAt, resolve });
    });

  const onRowUpdate = (newData, oldData) =>
    new Promise((resolve, reject) => {
      // Copy current state data to a new array
      const data = [...gridData.data];
      // Get edited row index
      const index = data.indexOf(oldData);
      // replace old data
      data[index] = newData;
      // update state with the new array
      const updatedAt = new Date();
      setGridData({ ...gridData, data, updatedAt, resolve });
    });



  return (
      <div className='table-ui'>
    <MaterialTable
    title="Recorders"
    options={{
        search:true,
        paging:false,
        filtering:true,
        exportButton:true,
        importButton:true,
        selection:true,
        
    }}  

      columns={gridData.columns}
      data={gridData.data}
      editable={{
        isEditable: rowData => true,
     
        onRowAdd: onRowAdd,
        onRowUpdate: onRowUpdate,
        
      }}
    />
    </div>
  );
};

export default Table;
