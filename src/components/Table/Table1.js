import React, { useState, useEffect } from 'react';
import MaterialTable, { MTableBodyRow } from 'material-table';


const Table = (props) => {

    const data =[
        {
           id:1, recorder:'Recorder 01', migration:'Migrated',  workstation:'WK000001', recorderstatus:'ONLINE'
        },
        {
            id:2, recorder:'Recorder 02', migration:'Migrated', workstation:'WK000002', recorderstatus:'ONLINE'
        },
        {
            id:3, recorder:'Recorder 03', migration:'Migrated', workstation:'WK000003', recorderstatus:'ONLINE'
        },
        {
            id:4, recorder:'Recorder 04', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
        },
        {
            id:5, recorder:'Recorder 05', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
        },
        {
            id:6, recorder:'Recorder 06', migration:'Non-Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
        },
        {
            recorder:'Recorder 07', migration:'Migrated', workstation:'WK000004', recorderstatus:'OFLINE'
        },
        {
            recorder:'Recorder 08', migration:'Migrated', workstation:'WK000004', recorderstatus:'OFLINE'
        },
        {
            recorder:'Recorder 09', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
        },
        {
            recorder:'Recorder 10', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
        },
        {
            recorder:'Recorder 11', migration:'Migrated', workstation:'WK000004', recorderstatus:'OFLINE'
        },
        {
            recorder:'Recorder 12', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
        },
        {
            recorder:'Recorder 13', migration:'Migrated', workstation:'WK000004', recorderstatus:'OFLINE'
        },
        {
            recorder:'Recorder 14', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
        }
    ]

    const columns =[
        {
            title:'Record Name', field:'recorder'
        },
        {
            title:'Migration Status', field:'migration'
        },
        {
            title:'Workstation', field:'workstation'
        },
        {
            title:'Recorder Status', field:'recorderstatus'
        }
    ]

    const [gridData, setGridData] = useState({
        data: data,
        columns: columns,
        resolve: () => {},
        updatedAt: new Date()
      });
    
      useEffect(() => {
        gridData.resolve();
        console.log("RESOLVE AT:", gridData.updatedAt);
      }, [gridData]);
    
      const onRowAdd = newData =>
        new Promise((resolve, reject) => {
          const data = [...gridData.data];
          data.push(newData);
          const updatedAt = new Date();
          setGridData({ ...gridData, data, updatedAt, resolve });
        });
    
      const onRowUpdate = (newData, oldData) =>
        new Promise((resolve, reject) => {
          // Copy current state data to a new array
          const data = [...gridData.data];
          // Get edited row index
          const index = data.indexOf(oldData);
          // replace old data
          data[index] = newData;
          // update state with the new array
          const updatedAt = new Date();
          setGridData({ ...gridData, data, updatedAt, resolve });
        });
    
      const onRowDelete = oldData =>
        new Promise((resolve, reject) => {
          let data = [...gridData.data];
          const index = data.indexOf(oldData);
          data.splice(index, 1);
          const updatedAt = new Date();
          setGridData({ ...gridData, data, updatedAt, resolve });
        });
    return (
        <div>
            <MaterialTable 
            title="Recorders"
            data={data}
            columns={columns}
            options={{
                search:true,
                paging:false,
                filtering:false,
                exportButton:true,
                importButton:true,
                selection:true
            }}  

           
            editable={{
                isEditable: rowData => true,
                isDeletable: rowData => true,
                onRowAdd: onRowAdd,
                onRowUpdate: onRowUpdate,
                onRowDelete: onRowDelete
              }}
             />
         
        </div>
    )
}

export default Table
