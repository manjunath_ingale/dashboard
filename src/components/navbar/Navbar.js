import React from 'react';
import './Navbar.css';
import Abhi from '../assets/abhi.jpg'

const Navbar = ({sidebarOpen, openSidebar}) => {
    return (
        <nav className="navbar">
<div className="nav_icon" onClick={()=> openSidebar()}>
    <i className="fa fa-bars"></i>
</div>
<div className="navbar__left">
<input type="text" id="searchInput" placeholder="Search.." />
</div>

<div className="navbar__right">
<a href=""><i className="fa fa-bell"></i></a>

<a href=""><img width="30" src={Abhi} alt='image' ></img></a>

</div>

        </nav>
    )
}

export default Navbar
