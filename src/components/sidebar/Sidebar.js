import React from 'react';
import './Sidebar.css';

const Sidebar = ({sidebarOpen, closeSidebar}) => {
    return (
        <div className={sidebarOpen ? "sidebar-responsive" : ""} id="sidebar">
            <div className="sidebar__title">
                <div className="sidebar__img">
{/* <img src="/" alt="logo"></img> */}
<h1>LNVR</h1>

                </div>
                <i className="fa fa-times" id="sidebarIcon" onClick={() => closeSidebar()}></i>

            </div>
            <div className="sidebar__menu">
                <div className="sidebar__link active_menu_link">
<i className="fa fa-home"></i>
<a href="#">Home Page</a>

                </div>
                
                <div className="sidebar__link">
                    <i className="fa fa-qrcode"></i>
                    <a href="#">Dashboard</a>
                </div>
                <div className="sidebar__link">
                    <i className="fa fa-user"></i>
                    <a href="#">Password</a>
                </div>

                <div className="sidebar__link">
                    <i className="fa fa-commenting"></i>
                    <a href="#">Help</a>
                </div>
                <div className="sidebar__link">
                    <i className="fa fa-gear"></i>
                    <a href="#">Settings</a>
                </div>
               
            </div>
        </div>
    )
}

export default Sidebar
