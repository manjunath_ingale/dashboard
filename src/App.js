
import  React, {useState} from 'react';
import Navbar from './components/navbar/Navbar'
import Sidebar from './components/sidebar/Sidebar';
import Table from './components/Table/Table';

const App = () => {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  const openSidebar = () =>{
    setSidebarOpen(true);
  }

  const closeSidebar = () =>{

    setSidebarOpen(false);
  }
  
  
 
  

  const data =[
    {
       id:1, recorder:'Recorder 01', migration:'Migrated',  workstation:'WK000001', recorderstatus:'ONLINE'
    },
    {
        id:2, recorder:'Recorder 02', migration:'Migrated', workstation:'WK000002', recorderstatus:'ONLINE'
    },
    {
        id:3, recorder:'Recorder 03', migration:'Migrated', workstation:'WK000003', recorderstatus:'ONLINE'
    },
    {
        id:4, recorder:'Recorder 04', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
    },
    {
        id:5, recorder:'Recorder 05', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
    },
    {
        id:6, recorder:'Recorder 06', migration:'Non-Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
    },
    {
        recorder:'Recorder 07', migration:'Migrated', workstation:'WK000004', recorderstatus:'OFLINE'
    },
    {
        recorder:'Recorder 08', migration:'Migrated', workstation:'WK000004', recorderstatus:'OFLINE'
    },
    {
        recorder:'Recorder 09', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
    },
    {
        recorder:'Recorder 10', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
    },
    {
        recorder:'Recorder 11', migration:'Migrated', workstation:'WK000004', recorderstatus:'OFLINE'
    },
    {
        recorder:'Recorder 12', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
    },
    {
        recorder:'Recorder 13', migration:'Migrated', workstation:'WK000004', recorderstatus:'OFLINE'
    },
    {
        recorder:'Recorder 14', migration:'Migrated', workstation:'WK000004', recorderstatus:'ONLINE'
    }
]

const comonscol =[
    {
        title:'Record Name', field:'recorder'
    },
    {
        title:'Migration Status', field:'migration'
    },
    {
        title:'Workstation', field:'workstation'
    },
    {
        title:'Recorder Status', field:'recorderstatus'
    }
]

  return (
    <div className='container'>
      <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
<Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar}/>

<Table col={comonscol} data={data} />
    </div>
  );
}

export default App;
